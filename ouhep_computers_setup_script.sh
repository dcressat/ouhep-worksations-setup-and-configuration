#!/bin/bash
# set flag to exit the script on any error
# set -e
# set -xv for debuging
# set -xv

echo "########################"
echo "Adding timestamp to user root bash history"
echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile

echo "########################"
echo "Running yum update"
yum -y update

echo "########################"
echo "Installing cvmfs"
yum -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
yum -y install cvmfs-fuse3.x86_64
locmap --enable cvmfs
locmap --configure cvmfs

echo "########################"
echo "Configuring /etc/cvmfs/default.local"
cat <<EOF_CVMFS_Config> /etc/cvmfs/default.local
# cvmfs default.local file installed with puppet
# this files overrides and extends the values contained
# within the default.conf file.

CVMFS_QUOTA_LIMIT='20000'
CVMFS_HTTP_PROXY="http://ca-proxy.cern.ch:3128"
CVMFS_CACHE_BASE='/var/lib/cvmfs'
CVMFS_REPOSITORIES='alice-ocdb.cern.ch,alice.cern.ch,ams.cern.ch,atlas-condb.cern.ch,atlas-nightlies.cern.ch,atlas.cern.ch,bbp.epfl.ch,cms.cern.ch,geant4.cern.ch,ilc.desy.de,lhcb.cern.ch,na61.cern.ch,sft.cern.ch'
EOF_CVMFS_Config

echo "########################"
echo "Restarting autofs to check that CVMFS works"
systemctl restart autofs.service
echo "########################"
echo "Checking that CVMFS works"
cvmfs_config probe

echo "########################"
echo "Enabling EOS"
locmap --enable eosclient
locmap --configure eosclient

echo "########################"
echo "Enabling RPM Fusion to support the installation of non-free software"
yum -y localinstall --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm

echo "########################"
echo "Installing the K Destop environment and Cinnamon Destop environment"
yum -y groupinstall "KDE Plasma Workspaces"
yum -y install kate konsole

echo "########################"
echo "Installing the Remmina RDP client"
yum -y install nux-dextop-release
yum -y install remmina-plugins-rdp.x86_64

echo "########################"
echo "Installing the Sublime text editor"
rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
yum-config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
yum -y install sublime-text

echo "########################"
echo "Installing java open jdk"
yum -y install java-1.8.0-openjdk-devel.x86_64

echo "########################"
echo "Installing additional software like htop screen, mc, the evolution mail client support for Exchange"
yum -y install evolution-ews evolution-ews-langpacks mc lnav terminator chromium screen wget chromium htop iotop

########## Disks and partitions management
echo "########################"
echo "Creating UserDisk1 mount point and remap the home partition to => /UserDisk1"
cd /
mkdir /UserDisk1
cp /etc/fstab /etc/fstab-backup
umount /home
sed -i 's/\/home/\/UserDisk1/g' /etc/fstab
mount /UserDisk1
# Changing /UserDisk1 mod to 777 to allow rwx file operations when the the directory is mounted over NFS
chmod 777 -R /UserDisk1

echo "########################"
echo "Creating UserDisk2 directory"
cd /
mkdir /UserDisk2
# Changing /UserDisk2 mod to 777 to allow rwx file operations when the the directory is mounted over NFS
chmod 777 -R /UserDisk2

echo "########################"
echo "Adding /UserDisk2 mount in /etc/fstab"
echo "/dev/sdb1                  /UserDisk2               ext4    defaults        0 0" >> /etc/fstab

echo "########################"
echo "Creating Disk1 and Disk2 mount points"
cd /
mkdir /Disk1 && mkdir /Disk2

echo "########################"
echo "Installing NFS"
yum install portmap nfs-utils nfs4-acl-tools

echo "########################"
echo "Configuring /etc/exports"
cat <<EOF_NFS_Exports_Config> /etc/exports
/UserDisk1      ouhep*.cern.ch(rw)
/UserDisk2      ouhep*.cern.ch(rw)
EOF_NFS_Exports_Config

echo "########################"
echo "Restarting and enabling the NFS service"
systemctl start rpcbind nfs-idmap nfs-server
systemctl enable rpcbind nfs-idmap nfs-server

echo "########################"
echo "### Configuring the firewall to allow clients to access the NFS share"
systemctl restart firewalld.service
firewall-cmd --permanent --add-service mountd && firewall-cmd --permanent --add-service rpc-bind && firewall-cmd --permanent --add-service nfs && firewall-cmd --reload

echo "########################"
echo "Configuring /etc/auto.master"
cat <<EOF_NFS_Auto_Master>> /etc/auto.master
/Disk1 /etc/auto.Disk1
/Disk2 /etc/auto.Disk2
EOF_NFS_Auto_Master

echo "########################"
echo "Configuring /etc/auto.Disk1"
cat <<EOF_NFS_Auto_Disk1> /etc/auto.Disk1
* &:/UserDisk1
EOF_NFS_Auto_Disk1

echo "##########"
echo "Configuring /etc/auto.Disk2"
cat <<EOF_NFS_Auto_Disk2> /etc/auto.Disk2
* &:/UserDisk2
EOF_NFS_Auto_Disk2

echo "########################"
echo "Restarting autofs"
systemctl restart autofs.service

########## Users management
echo "########################"
echo "Give Horst Severin sudo rights"
usermod -G wheel severini
