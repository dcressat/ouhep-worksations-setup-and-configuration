# OUHEP Workstations Configuration And Post-install Script

## Typical Hardware Setup:

- A 256 Gig disk on which CERN CentOS 7 is installed and on which the `/home` partition is remapped  `/UserDisk1` by the `ouhep_computers_setup_script.sh` shell script.
As per the default installation of CERN Centos the `/boot` `/` and `/home` partitions uses the XFS file system.
- A 2nd disk of 4 TB with a single partition using the EXT4 file system, which is mounted under `/UserDisk2` and is used as temporary local space by the students for storing their work data. However while the `ouhep_computers_setup_script.sh` shell script will create the `/UserDisk2` mount point, at the time of  writing, the 2nd disk still needs to be added as a final step after the `ouhep_computers_setup_script.sh` has been run.

## Typical `/etc/fstab`:

```
# Created by anaconda on Tue Mar 16 15:53:52 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk'
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info
#
/dev/mapper/cc_ouhep07-root /                       xfs     defaults        0 0
UUID=713794ba-07e0-4e44-8b06-4df6f753b495 /boot     xfs     defaults        0 0
/dev/mapper/cc_ouhep07-home /UserDisk1              xfs     defaults        0 0
/dev/mapper/cc_ouhep07-swap swap                    swap    defaults        0 0
/dev/sdb1                  /UserDisk2               ext4    defaults        0 0
```

---

## Typical software installation and configuration.

Following a normal installation of CERN Centos 7, the following `ouhep_computers_setup_script.sh` Post-install script (see below), will takes care of:

- Re-mapping the `/home` partition to `/UserDisk1` mount point
- Create the `/UserDisk2` mount point.
- Adding date and time stamping to the `root` user history file in order to more easily keep track of what was done and when by any user having executed any `sudo` commands.
- Installing and configuring EOS and CVMFS
- Authentication with SSSD, which dispenses from having to manually add students logging to the computer using the 'addusercern` command.
- Exporting the `UserDisk1` and `UserDisk2` mount points over NFS to any OUHEP* computers.
- Adding `/Disk` and `/Disk2` directories to mount the `UserDisk1` and `UserDisk2` mount points from other OUHEP computers
- Installing the KDE desktop environment which is a more modern, user friendlier and more productive desktop environments than Gnome
- Installing the evolution mail client which with with the EWS extension provide a better email client and easier to setup than Thunderbird and provides the CERN "Exchange Global address list"
- Installing [Chromium]([https://en.wikipedia.org/wiki/Chromium_(web_browser)](https://en.wikipedia.org/wiki/Chromium_(web_browser))), [screen]([[https://en.wikipedia.org/wiki/GNU_Screen](https://en.wikipedia.org/wiki/GNU_Screen)), [terminator]([https://en.wikipedia.org/wiki/Terminator_(terminal_emulator)](https://en.wikipedia.org/wiki/Terminator_(terminal_emulator))), [wget]([https://en.wikipedia.org/wiki/Wget](https://en.wikipedia.org/wiki/Wget)), [lnav]([http://lnav.org/](http://lnav.org/)) and more
- Running yum update to install any outstanding updates
- Giving user `severini`full `sudo` "super User" rights.

## NFS auto-mounting and cross-mounting UserDisk1 and UserDisk2
The way this auto-mounting works is this:
```
/etc/auto.master:
/Disk1 /etc/auto.Disk1

/etc/auto.Disk1:
* &:/UserDisk1
```

When you request `/Disk1/ouhep01/`, the `*` matches anything after `/Disk1/`,
meaning the hostname, and then the `&` gets replaced with that hostname.
So it becomes this:
`ouhep01 ouhep01:/UserDisk1`

Which means that `/Disk1/ouhep01` will be mounted as `ouhep01:/UserDisk1`

Example of mounting / creating a file on `ouhep08:/UserDisk1` from computer ouhep06.cern.ch
```
touch /Disk1/ouhep08.cern.ch/testfile
```

## How to use and run the script
To configure a computer, perform a standard installation of CERN Centos 7, reboot the computer and execute the following command as root:

```
curl -L https://gitlab.cern.ch/dcressat/ouhep-worksations-setup-and-configuration/-/raw/master/ouhep_computers_setup_script.sh?inline=false -o /tmp/ouhep_computers_setup_script.sh && chmod 755 /tmp/ouhep_computers_setup_script.sh && /tmp/ouhep_computers_setup_script.sh 2>&1 |tee /tmp/ouhep_computers_setup_script.log && reboot
```

## ouhep_computers_setup_script.sh details
```
#!/bin/bash
# set flag to exit the script on any error
# set -e
# set -xv for debuging
# set -xv

echo "########################"
echo "Adding timestamp to user root bash history"
echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile

echo "########################"
echo "Running yum update"
yum -y update

echo "########################"
echo "Installing cvmfs"
yum -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
yum -y install cvmfs-fuse3.x86_64
locmap --enable cvmfs
locmap --configure cvmfs

echo "########################"
echo "Configuring /etc/cvmfs/default.local"
cat <<EOF_CVMFS_Config> /etc/cvmfs/default.local
# cvmfs default.local file installed with puppet
# this files overrides and extends the values contained
# within the default.conf file.

CVMFS_QUOTA_LIMIT='20000'
CVMFS_HTTP_PROXY="http://ca-proxy.cern.ch:3128"
CVMFS_CACHE_BASE='/var/lib/cvmfs'
CVMFS_REPOSITORIES='alice-ocdb.cern.ch,alice.cern.ch,ams.cern.ch,atlas-condb.cern.ch,atlas-nightlies.cern.ch,atlas.cern.ch,bbp.epfl.ch,cms.cern.ch,geant4.cern.ch,ilc.desy.de,lhcb.cern.ch,na61.cern.ch,sft.cern.ch'
EOF_CVMFS_Config

echo "########################"
echo "Restarting autofs to check that CVMFS works"
systemctl restart autofs.service
echo "########################"
echo "Checking that CVMFS works"
cvmfs_config probe

echo "########################"
echo "Enabling EOS"
locmap --enable eosclient
locmap --configure eosclient

echo "########################"
echo "Setting up authentication with SSSD / LDAP"
wget http://linux.web.cern.ch/docs/sssd.conf.example -O /etc/sssd/sssd.conf
chown root:root /etc/sssd/sssd.conf
chmod 0600 /etc/sssd/sssd.conf
restorecon /etc/sssd/sssd.conf
authconfig --enablesssd --enablesssdauth --update
systemctl enable sssd
systemctl start sssd

echo "########################"
echo "Enabling RPM Fusion to support the installation of non-free software"
yum -y localinstall --nogpgcheck https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm

echo "########################"
echo "Installing the K Destop environment and Cinnamon Destop environment"
yum -y groupinstall "KDE Plasma Workspaces"
yum -y install kate konsole

echo "########################"
echo "Installing the Remmina RDP client"
yum -y install nux-dextop-release
yum -y install remmina-plugins-rdp.x86_64

echo "########################"
echo "Installing the Sublime text editor"
rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
yum-config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
yum -y install sublime-text

echo "########################"
echo "Installing java open jdk"
yum -y install java-1.8.0-openjdk-devel.x86_64

echo "########################"
echo "Installing additional software like htop screen, mc, the evolution mail client support for Exchange"
yum -y install evolution-ews evolution-ews-langpacks mc lnav terminator chromium screen wget chromium htop iotop

########## Disks and partitions management
echo "########################"
echo "Creating UserDisk1 mount point and remap the home partition to => /UserDisk1"
cd /
mkdir /UserDisk1
cp /etc/fstab /etc/fstab-backup
umount /home
sed -i 's/\/home/\/UserDisk1/g' /etc/fstab
mount /UserDisk1
# Changing /UserDisk1 mod to 777 to allow rwx file operations when the the directory is mounted over NFS
chmod 777 -R /UserDisk1

echo "########################"
echo "Creating UserDisk2 directory"
cd /
mkdir /UserDisk2
# Changing /UserDisk2 mod to 777 to allow rwx file operations when the the directory is mounted over NFS
chmod 777 -R /UserDisk2

echo "########################"
echo "Adding /UserDisk2 mount in /etc/fstab"
echo "/dev/sdb1                  /UserDisk2               ext4    defaults        0 0" >> /etc/fstab

echo "########################"
echo "Creating Disk1 and Disk2 mount points"
cd /
mkdir /Disk1 && mkdir /Disk2

echo "########################"
echo "Installing NFS"
yum install portmap nfs-utils nfs4-acl-tools

echo "########################"
echo "Configuring /etc/exports"
cat <<EOF_NFS_Exports_Config> /etc/exports
/UserDisk1      ouhep*.cern.ch(rw)
/UserDisk2      ouhep*.cern.ch(rw)
EOF_NFS_Exports_Config

echo "########################"
echo "Restarting and enabling the NFS service"
systemctl start rpcbind nfs-idmap nfs-server
systemctl enable rpcbind nfs-idmap nfs-server

echo "########################"
echo "### Configuring the firewall to allow clients to access the NFS share"
systemctl restart firewalld.service
firewall-cmd --permanent --add-service mountd && firewall-cmd --permanent --add-service rpc-bind && firewall-cmd --permanent --add-service nfs && firewall-cmd --reload

echo "########################"
echo "Configuring /etc/auto.master"
cat <<EOF_NFS_Auto_Master>> /etc/auto.master
/Disk1 /etc/auto.Disk1
/Disk2 /etc/auto.Disk2
EOF_NFS_Auto_Master

echo "########################"
echo "Configuring /etc/auto.Disk1"
cat <<EOF_NFS_Auto_Disk1> /etc/auto.Disk1
* &:/UserDisk1
EOF_NFS_Auto_Disk1

echo "##########"
echo "Configuring /etc/auto.Disk2"
cat <<EOF_NFS_Auto_Disk2> /etc/auto.Disk2
* &:/UserDisk2
EOF_NFS_Auto_Disk2

echo "########################"
echo "Restarting autofs"
systemctl restart autofs.service

########## Users management
echo "########################"
echo "Give Horst Severin sudo rights"
usermod -G wheel severini
```
